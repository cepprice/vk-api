import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Friends {

    static TransportClient transportClient = HttpTransportClient.getInstance();
    public static VkApiClient vk = new VkApiClient(transportClient);
    public static final int myProfileID = 172692422;

    public static UserActor actor = new UserActor(59234359,
            "cacb099b44e16a887a301468627e1b165009b7062ba951c0f215f193809d25418052880ba236aa3631217");

    public static void main(String[] args) throws ClientException, ApiException, InterruptedException, IOException {

        int profileId = getProfileId();

        List <Integer> listOfFriends = getFriends(profileId, 0);

        HashMap<Integer, List<Integer>> friendMap = makeFriendMap(listOfFriends);

        MatrixWriter writer = new MatrixWriter(listOfFriends, friendMap);
        writer.makeExcelTable("friends.xls");
    }

    private static int getProfileId() {
        System.out.println("Введите id пользователя (только цифры). Либо введите 0, чтобы использовать id автора программы:");
        Scanner scanner = new Scanner(System.in);
        int id = scanner.nextInt();
        return id == 0 ? myProfileID : id;
    }

    public static HashMap<Integer, List<Integer>> makeFriendMap(List<Integer> listOfFriends) throws ClientException, InterruptedException {

        HashMap<Integer, List<Integer>> map = new HashMap<>();

        for (int i = 0; i < listOfFriends.size() - 1; i++) {
            for (int j = i + 1; j < listOfFriends.size(); j++) {
                int friend1 = listOfFriends.get(i);
                int friend2 = listOfFriends.get(j);
                if (isFriend(friend1, friend2)) {
                    List<Integer> list = map.get(friend1);
                    if (list != null) {
                        list.add(friend2);
                        map.replace(friend1, list);
                    } else {
                        list = new ArrayList<>();
                        list.add(friend2);
                        map.put(friend1, list);
                    }

                    list = map.get(friend2);
                    if (list != null) {
                        list.add(friend1);
                        map.replace(friend2, list);
                    } else {
                        list = new ArrayList<>();
                        list.add(friend1);
                        map.put(friend2, list);
                    }
                }
            }
        }

        return map;
    }

    public static boolean isFriend(int left, int right) throws ClientException, InterruptedException {
        TimeUnit.MILLISECONDS.sleep(1);
        try {
            return vk.friends().getMutual(actor).sourceUid(myProfileID).targetUid(left).execute().contains(right);
            //return vk.friends().get(actor).userId(left).execute().getItems().contains(right);
        } catch (ApiException e) {
            TimeUnit.MILLISECONDS.sleep(1);
            return isFriend(left, right);
        }
    }

    static List<Integer> getFriends(int id, int amount) throws ClientException, ApiException {

        amount = amount == 0 ? 10000 : amount;

        return vk.friends()
                .get(actor)
                .userId(id)
                .count(amount)
                .execute()
                .getItems();
    }
}
