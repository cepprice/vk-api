import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class MatrixWriter {

    Workbook book = new HSSFWorkbook();
    Sheet sheet = book.createSheet();

    private HashMap<Integer, List<Integer>> friendMap;
    private List<Integer> listOfFriends;

    private final HashMap<String, CellStyle> styles = createStyles();

    private final String VK_URL = "https://vk.com/id";

    public MatrixWriter(List<Integer> friends, HashMap<Integer, List<Integer>> map) {
        listOfFriends = friends;
        friendMap = map;
    }

    public void makeExcelTable(String fileName) throws IOException {
        writeHorizontalIDs();
        writeVerticalIDs();
        writeFriendsConnections();
        writeChanges(fileName);
    }

    private void writeFriendsConnections() {
        int len = listOfFriends.size();
        for (int rowIndex = 1; rowIndex <= len; rowIndex++) {
            for (int columnIndex = 1; columnIndex <= len; columnIndex++) {
                writeCell(rowIndex, columnIndex);
            }
        }
    }

    private void writeCell(int rowIndex, int columnIndex) {
        Cell cell = sheet.getRow(rowIndex).createCell(columnIndex);

        boolean areFriends;
        try {
            areFriends = friendMap
                    .get(listOfFriends.get(rowIndex - 1))
                    .contains(listOfFriends.get(columnIndex - 1));
        } catch (NullPointerException e) {
            areFriends = false;
        }

        if (areFriends) {
            CellStyle style = book.createCellStyle();
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
            style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
            cell.setCellStyle(styles.get("GREEN"));
        }
    }

    private void writeVerticalIDs() {
        for (int i = 1; i <= listOfFriends.size(); i++) {
            Cell cell = sheet.createRow(i).createCell(0);
            cell.setCellValue(listOfFriends.get(i-1));
            cell.setHyperlink(makeHyperlink(listOfFriends.get(i-1).toString()));
        }
        sheet.autoSizeColumn(0);
    }

    private void writeChanges(String filePath) throws IOException {
        book.write(new FileOutputStream(filePath));
    }

    private void writeHorizontalIDs() {
        Row row = sheet.createRow(0);
        for (int i = 1; i <= listOfFriends.size(); i++) {
            row.createCell(i).setCellValue(listOfFriends.get(i-1).toString());
            row.getCell(i)
                    .setHyperlink(makeHyperlink(listOfFriends.get(i-1).toString()));
            sheet.autoSizeColumn(i);
        }
    }

    private Hyperlink makeHyperlink(String id) {
        Hyperlink link = book
                .getCreationHelper()
                .createHyperlink(org.apache.poi.ss.usermodel.Hyperlink.LINK_URL);
        link.setAddress(VK_URL + id);
        return link;
    }

    private HashMap<String, CellStyle> createStyles() {
        HashMap<String, CellStyle> styles = new HashMap<>();

        CellStyle style = book.createCellStyle();
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);

        style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
        styles.put("GREEN", style);

        style.setFillBackgroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
        styles.put("GREY", style);

        style.setFillBackgroundColor(IndexedColors.WHITE.getIndex());
        styles.put("WHITE", style);

        return styles;
    }
}
