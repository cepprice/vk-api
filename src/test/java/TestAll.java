import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.friends.FriendStatus;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.formula.functions.Column;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestAll {

    Workbook book = new HSSFWorkbook();
    Sheet sheet = book.createSheet();

    static TransportClient transportClient = HttpTransportClient.getInstance();
    public static VkApiClient vk = new VkApiClient(transportClient);

    public static UserActor actor = new UserActor(17605599,
            "cacb099b44e16a887a301468627e1b165009b7062ba951c0f215f193809d25418052880ba236aa3631217");



    public static void main(String[] args) throws ClientException, ApiException {
        final int right = 221651260;
        final int left = 17605599;
        List<FriendStatus> res = vk.friends().areFriends(actor, left, right).execute();
        System.out.println(res.get(0).getFriendStatus().getValue());

    }

    @Test
    public void rustamErnest() throws ClientException, ApiException, InterruptedException {
        final int right = 59234359;
        final int left = 66129190;

        boolean result = Friends.isFriend(left, right);

        assert !result;
    }

    @Test
    public void meRustam() throws ClientException, InterruptedException {
        final int me = 172692422;
        final int rustam = 59234359;

        boolean result = Friends.isFriend(me, rustam);

        assert result;
    }

    @Test
    public void RenatDanil() throws ClientException, InterruptedException {
        final int danil = 121417603;
        final int renat = 127746635;

        boolean result = Friends.isFriend(danil, renat);
        assert result;
    }

    @Test
    public void mapOfRenatDanilDenis() throws ClientException, InterruptedException, ApiException {
        final int danil = 121417603;
        final int renat = 127746635;
        final int denis = 136414393;

        ArrayList<Integer> list = new ArrayList<>();
        list.add(danil);
        list.add(renat);
        list.add(denis);

        HashMap<Integer, List<Integer>> map = Friends.makeFriendMap(list);

        for (int key : map.keySet()) {
            System.out.println(key + ": " + map.get(key));
        }
    }

    @Test
    public void xls() throws InterruptedException, ClientException, ApiException, IOException {
        final int danil = 121417603;
        final int renat = 127746635;
        final int denis = 136414393;

        ArrayList<Integer> list = new ArrayList<>();
        list.add(danil);
        list.add(renat);
        list.add(denis);

        HashMap<Integer, List<Integer>> map = Friends.makeFriendMap(list);
        MatrixWriter writer = new MatrixWriter(list, map);
        writer.makeExcelTable("friends.xls");
    }

    @Test
    public void changeCellColor() throws IOException {
        CellStyle style = book.createCellStyle();
        style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellStyle(style);
        cell.setCellValue("Hello");

        FileOutputStream out = new FileOutputStream("C:\\Users\\Kozak\\Desktop\\test.xls");
        book.write(out);
        out.close();
    }

}
